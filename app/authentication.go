package app

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	u "../utils" //"go-contacts/utils"
	"os"
	"strings"

)

var JwtAuthentication = func(next http.Handler) http.Handler{

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			notAuth := []string{"/api/user/new", "/api/user/login"} // the endpoint
			requestPath := r.URL.Path // request way

			for _,value := range notAuth {

				if value == requestPath {
					next.ServeHTTP(w, r)
					return
				}
			}

			response := make(map[string]interface{})
			tokenHeader := r.Header.Get("Autorization") // get Token

			if tokenHeader == "" {
				response = u.Message(false, "Missing aunt token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				u.Respond(w.response)
				return
			}

			splitted := strings.Split(tokenHeader, " ")
			if len(splitted) != 2{
				response = u.Message(false, "Invalid/Malformed auth token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				u.Respond(w, response)
				return
			}

			tokenPart := splitted[1] // receiving the second part of the token
			tk := &models.Token{}

			token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
				return []byte(os.Getenv("token_password")), nil
			})

			if err != nil { //Wrong Token
				response = u.Message(false, "Malformed authentication token")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				u.Respond(w, response)
				return
			}

			if !token.Valid { //the token is invalid
				response = u.Message(false, "Token is not valid.")
				w.WriteHeader(http.StatusForbidden)
				w.Header().Add("Content-Type", "application/json")
				u.Respond(w, response)
				return
			}

			// Everything went well, continue query execution
			fmt.Sprintf("User %", tk.Username) //Полезно для мониторинга
			ctx := context.WithValue(r.Context(), "user", tk.UserId)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r) // transfer control to the next handler!
	})

}
